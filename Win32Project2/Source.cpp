//#include <SFML/Graphics.hpp>
#include <vector>
#include "D:\prog\Konvoi\Konvoi\Konvoi.h"
#include <iostream>
#include "BigBoss.h"


int main()
{
	RenderWindow window(VideoMode(1000, 700), "Hochy zachot!");
	
	Music music;

	music.openFromFile("Music.ogg");
	music.play();
	music.setLoop(true);

	
	Paints paints;
	Sprite tileFon(paints.tileSetFon);
	Clock clock;
	tileFon.setTextureRect(IntRect(0, 0, 1000, 700));
	
	
	window.clear();
	window.draw(tileFon);
	window.display();

	Text text;
	text.setFont(paints.font);
		
	while (window.isOpen())
	{
		BigBoss BOSS(paints);
		
		BOSS.StartMenu(window, tileFon,text);
		
		clock.restart();
		
		do
		{
			double time = clock.getElapsedTime().asMicroseconds();
			BOSS.mayBePause(text, window,tileFon);
			
			clock.restart();
			
			
			BOSS.timeScore(time, text, BOSS.player);
			Event event;

			while (window.pollEvent(event))
			{
				if (event.type == Event::Closed)
					window.close();
			}

			if (BOSS.player.dead)
				break;
			

			BOSS.mayBeNew(paints, time, BOSS.player.Score);
			
			BOSS.player.keys(paints, BOSS.A_P,BOSS.sounds);

			BOSS.update(time,text);
			
			BOSS.A_E.shot(BOSS.A_E_P, paints,BOSS.sounds);
			
			BOSS.draw(window, tileFon, text);

			BOSS.Collision(paints);

			

		} while (1);
	}
	return 0;

}