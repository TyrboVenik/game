#include "Patron.h"



Patron::Patron(Texture &image, int duloX, int duloY)
{
	ShotVel = 0.0009;
	dx = duloX;
	dy = duloY;
	dead = 0;
	sprite.setTexture(image);
	rect = FloatRect(0, 0, 20, 20);
}

void Patron::update(float time)
{
	if (rect.left <= 0 || rect.left >= 1000 || rect.top <= 0 || rect.top >= 700)
		dead = 1;
	rect.left += ShotVel*dx*time;
	rect.top += ShotVel*dy*time;

	sprite.setPosition(rect.left, rect.top);
}