#pragma once
#include <SFML/Audio.hpp>
using namespace sf;
class Sounds
{
public:
	SoundBuffer bufferLazer;
	Sound soundLazer;

	SoundBuffer bufferShot;
	Sound soundShot;

	SoundBuffer bufferBigBang;
	Sound soundBigBang;

	Sounds()
	{
		bufferLazer.loadFromFile("Lazer.ogg");
		soundLazer.setBuffer(bufferLazer);
		int vol = soundLazer.getVolume();
		soundLazer.setBuffer(bufferLazer);
		soundLazer.setVolume(vol / 3);


		bufferShot.loadFromFile("Shot.ogg");
		vol = soundShot.getVolume();
		soundShot.setBuffer(bufferShot);
		soundShot.setVolume(vol/20);

		bufferBigBang.loadFromFile("BigBang.ogg");
		soundBigBang.setBuffer(bufferBigBang);
	}
	
};

