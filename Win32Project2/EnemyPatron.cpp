#include "EnemyPatron.h"


EnemyPatron::EnemyPatron(Texture &image, const float& L, const float& R)
{
	dead = 0;
	sprite.setTexture(image);
	rect = FloatRect(0, 0, L, R);
	DeadTime = 0;
};

void EnemyPatron::update(float time)
{
	DeadTime += time;
}