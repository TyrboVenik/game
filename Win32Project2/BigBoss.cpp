#include "BigBoss.h"

void BigBoss::PauseMenu(RenderWindow & window, Sprite & tileFon, Text & text)
{
	Clock clock;

	Texture menu_texture1, menu_texture2, menu_texture3, about_texture;
	menu_texture1.loadFromFile("111.png");
	menu_texture2.loadFromFile("222.png");
	menu_texture3.loadFromFile("333.png");
	about_texture.loadFromFile("about.png");
	Sprite menu2(menu_texture2),menu1(menu_texture1), menu3(menu_texture3), about(about_texture);
	bool Menu = 1;
	int MenuNum = 0;
	menu2.setPosition(100, 30);
	menu1.setPosition(100, 90);
	menu3.setPosition(100, 150);
	about.setPosition(300, 200);
	about.setColor(Color::Yellow);

	double tau = 0, maxtau = 80000;
	int ready = 1;

	/////����
	while (Menu)
	{
		menu1.setColor(Color::White);
		menu2.setColor(Color::White);
		menu3.setColor(Color::White);
		//MenuNum = 0;
		//window.clear(Color(0, 0, 0));
		double time = clock.getElapsedTime().asMicroseconds();
		clock.restart();

		if (!ready)
		{
			tau += time;
			if (tau > maxtau)
			{
				tau = 0;
				ready = 1;
			}
			continue;
		}
		ready = 0;

		if (Keyboard::isKeyPressed(Keyboard::Down))
		{
			MenuNum++;
			if (MenuNum > 2)
				MenuNum = 0;
		}

		if (Keyboard::isKeyPressed(Keyboard::Up))
		{
			MenuNum--;
			if (MenuNum < 0)
				MenuNum = 2;
		}

		if (MenuNum == 0)
			menu2.setColor(Color::Yellow);
		if (MenuNum == 1)
			menu1.setColor(Color::Yellow);
		if (MenuNum == 2)
			menu3.setColor(Color::Yellow);


		if (Keyboard::isKeyPressed(Keyboard::Return))
		{
			if (MenuNum == 0) Menu = false;
			if (MenuNum == 1)
			{
				Menu = false;
				player.dead = 1;
			}
			if (MenuNum == 2) { window.close(); exit(0); }
		}

		//window.draw(tileFon);
		draw(window, tileFon, text);
		window.draw(about);
		window.draw(menu1);
		window.draw(menu2);
		window.draw(menu3);


		window.display();
	}
}

void BigBoss::mayBePause(Text&text, RenderWindow&window,Sprite&tileFon)
{
	if (Keyboard::isKeyPressed(Keyboard::P))
		PauseMenu(window, tileFon, text);
	/*
	if (Keyboard::isKeyPressed(Keyboard::P))
	{
		string S1("DONT STOP PLAY SYKA,PRESS \"P\"");

		text.setString(S1);
		text.setCharacterSize(50);
		text.setColor(Color::Red);
		text.setStyle(Text::Bold | Text::Underlined);


		window.draw(text);
		window.display();

		while (1)
			if (!Keyboard::isKeyPressed(Keyboard::P))
				break;

		while (1)
			if (Keyboard::isKeyPressed(Keyboard::P))
				break;

		while (1)
			if (!Keyboard::isKeyPressed(Keyboard::P))
				break;
	}
	*/
}

void BigBoss::waitPressR(RenderWindow&window, Text&text)
{
	string S1("PRESS R TO START,SYKA");
	text.setString(S1);
	text.setCharacterSize(50);
	text.setColor(Color::Red);
	text.setStyle(Text::Bold | Text::Underlined);
	window.draw(text);
	window.display();

	while (1)
		if (!Keyboard::isKeyPressed(Keyboard::R))
			break;

	while (1)
		if (Keyboard::isKeyPressed(Keyboard::R))
			break;

	while (1)
		if (!Keyboard::isKeyPressed(Keyboard::R))
			break;
}

void BigBoss::mayBeNew(Paints&paints, double time, double Score)
{
	stones.MayBeNew(paints.tileMet, player.Score);
	A_E.MayBeNew(paints, player.Score);
	A_E.MayBeNew(paints, player.Score);
}

void BigBoss::update(double time, Text&text)
{
	timeScore(time, text, player);
	player.update(time);
	stones.update(time, player.Score);
	A_E.update(time);
	A_P.update(time);
	A_E_P.update(time);
}

void BigBoss::Collision(Paints&paints)
{
	for (int i = 0; i < stones.arr.size(); ++i)
	{
		if (player.rect.intersects(stones.arr[i].rect))
		{
			if (stones.arr[i].dead)
				continue;

			player.dead = 1;
			player.sprite.setColor(Color::Red);
			stones.arr[i].sprite.setTexture(paints.tileMetBang);
			stones.arr[i].sprite.setTextureRect(IntRect(0, 0, 50, 50));
			stones.arr[i].dead = 1;
			stones.arr[i].dx = 0;
			stones.arr[i].dy = 0;
			stones.arr[i].DeadTime = 0;
		}
	}
	for (int i = 0; i < stones.arr.size(); ++i)
		for (int j = 0; j < A_P.arr.size(); ++j)
			if (stones.arr[i].rect.intersects(A_P.arr[j].rect))
			{
				if (stones.arr[i].dead)
					continue;

				

				stones.arr[i].sprite.setTexture(paints.tileMetBang);
				stones.arr[i].sprite.setTextureRect(IntRect(0, 0, 100, 100));
				stones.arr[i].dead = 1;
				stones.arr[i].dx = 0;
				stones.arr[i].dy = 0;
				stones.arr[i].DeadTime = 0;
				A_P.arr.erase(A_P.arr.begin() + j);
				--j;
			}

	for (int i = 0; i < A_P.arr.size(); ++i)
		for (int j = 0; j < A_E.arr.size(); ++j)
		{
			if (A_P.arr[i].rect.intersects(A_E.arr[j].rect))
			{
				if (A_E.arr[j].dead)
					continue;

				A_E.arr[j].hp -= 1;
				A_P.arr.erase(A_P.arr.begin() + i);
				--i;
				if (A_E.arr[j].hp == 0)
				{
					A_E.arr[j].dead = 1;
					sounds.soundBigBang.play();
					A_E.arr[j].sprite.setTexture(paints.tileEnemyBang);
					A_E.arr[j].sprite.setTextureRect(IntRect(0, 0, 100, 100));
				}
				break;
			}
		}

	for (int i = 0; i < A_E_P.arr.size(); ++i)
	{
		//cout << A_E_P.arr[i].rect.top << "  " << A_E_P.arr[i].rect.left <<"   "<<  A_E_P.arr[i].rect.height<<"   "<< A_E_P.arr[i].rect.width << endl;
		if (player.rect.intersects(A_E_P.arr[i].rect))
		{
			player.dead = 1;
			player.sprite.setColor(Color::Red);
			return;
		}
		for (int j = 0; j < stones.arr.size(); ++j)
		{
			if (stones.arr[j].rect.intersects(A_E_P.arr[i].rect))
			{
				if (stones.arr[j].dead)
					continue;
				
				stones.arr[j].sprite.setTexture(paints.tileMetBang);
				stones.arr[j].sprite.setTextureRect(IntRect(0, 0, 100, 100));
				stones.arr[j].dead = 1;
				stones.arr[j].dx = 0;
				stones.arr[j].dy = 0;
				stones.arr[j].DeadTime = 0;
				--j;
			}
		}
	}

}

void BigBoss::draw(RenderWindow&window, Sprite&fon, Text&text)
{
	window.clear();
	window.draw(fon);
	window.draw(player.sprite);

	window.draw(text);
	stones.drawAll(window);
	A_E.draw(window);
	A_E_P.draw(window);
	A_P.drawAll(window);

	window.display();
}

void BigBoss::timeScore(double time, Text& text, Player&player)
{
	string S1("Score:");
	player.Score += time / 1000000;
	char s[100];
	_itoa_s(int(player.Score), s, 100, 10);
	string S(s);
	S1 += S;
	text.setString(S1);
	text.setCharacterSize(24);
	text.setColor(sf::Color::Red);
	text.setStyle(sf::Text::Bold | sf::Text::Underlined);
}

void BigBoss::StartMenu(RenderWindow&window, Sprite&tileFon,Text&text)
{
	Clock clock;

	Texture menu_texture1,menu_texture3, about_texture;
	menu_texture1.loadFromFile("111.png");
	menu_texture3.loadFromFile("333.png");
	about_texture.loadFromFile("about.png");
	Sprite menu1(menu_texture1), menu3(menu_texture3), about(about_texture);
	bool Menu = 1;
	int MenuNum = 0;
	menu1.setPosition(100, 90);
	menu3.setPosition(100, 150);
	about.setPosition(300, 200);
	about.setColor(Color::Yellow);

	double tau = 0, maxtau = 80000;
	int ready = 1;

	/////����
	while (Menu)
	{
		menu1.setColor(Color::White);
		menu3.setColor(Color::White);
		//MenuNum = 0;
		//window.clear(Color(0, 0, 0));
		double time = clock.getElapsedTime().asMicroseconds();
		clock.restart();

		if (!ready)
		{
			tau += time;
			if (tau > maxtau)
			{
				tau = 0;
				ready = 1;
			}
			continue;
		}
		ready = 0;

		if (Keyboard::isKeyPressed(Keyboard::Down))
		{
			MenuNum++;
			if (MenuNum > 1)
				MenuNum = 0;
		}

		if (Keyboard::isKeyPressed(Keyboard::Up))
		{
			MenuNum--;
			if (MenuNum < 0)
				MenuNum = 1;
		}

		if (MenuNum == 0)
			menu1.setColor(Color::Yellow);
		if (MenuNum == 1)
			menu3.setColor(Color::Yellow);


		if (Keyboard::isKeyPressed(Keyboard::Return))
		{
			if (MenuNum == 0) Menu = false;
			if (MenuNum == 1) { window.close(); exit(0); }
		}

		//window.draw(tileFon);
		draw(window, tileFon,text);
		window.draw(about);
		window.draw(menu1);
		window.draw(menu3);


		window.display();
	}
}
/*
void BigBoss::menuDead(RenderWindow & window,Sprite&tileFon)
{
	draw(window,tileFon);
}
*/




