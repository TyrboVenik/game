#include "Meteor.h"
#include <iostream>

Meteor::Meteor(Texture &image,float Score)
{
	StartMeteorVel = 0.00007;
	Acceleration = 0.00001;
	MeteorVel = StartMeteorVel + Score*Acceleration/10;
	currentFrame = 0;
	dead = 0;
	sprite.setTexture(image);
	rect = FloatRect(0, 0, 50, 50);
	if (rand() % 2 == 0)
		dx = MeteorVel;
	else
		dx = -MeteorVel;
	if (rand() % 2 == 0)
		dy = MeteorVel;
	else
		dy = -MeteorVel;
	int i = rand() % 4;

	if (i == 0)
	{
		rect.left = 0;
		rect.top = rand() % 650;
	}

	if (i == 1)
	{
		rect.left = rand() % 950;
		rect.top = 0;
	}

	if (i == 2)
	{
		rect.left = 950;
		rect.top = rand() % 650;
	}

	if (i == 3)
	{
		rect.left = rand() % 950;
		rect.top = 650;
	}
}

void Meteor::update(float time,float Score)
{
	if (dead)
	{
		DeadTime += time;
		IntRect re(int(DeadTime * 12 / 1000000) * 100, 0, 50, 50);
		sprite.setTextureRect(re);
		return;
	}
	if (Score<80)
		MeteorVel = StartMeteorVel + Score*Acceleration/10;
	
	Collision();

	rect.left += dx*time;

	rect.top += dy*time;

	sprite.setPosition(rect.left, rect.top);

}

void Meteor::Collision()
{
	if (rect.left <= 0)
	{
		dx = -dx;
		rect.left += MeteorVel;
	}

	if (rect.left >= 950)
	{
		dx = -dx;
		rect.left -= MeteorVel;
	}

	if (rect.top <= 0)
	{
		dy = -dy;
		rect.top += MeteorVel;
	}

	if (rect.top >= 650)
	{
		dy = -dy;
		rect.top -= MeteorVel;
	}
}