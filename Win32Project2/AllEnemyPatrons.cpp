#include "AllEnemyPatrons.h"


void AllEnemyPatrons::update(float time)
{

	for (int i = 0; i < arr.size(); ++i)
	{
		arr[i].update(time);
		if (arr[i].DeadTime > MaxEnemyFireDeadTime)
		{
			arr.erase(arr.begin() + i);
			--i;
		}
	}
}

void AllEnemyPatrons::draw(RenderWindow&window)
{
	for (int i = 0; i < arr.size(); ++i)
		window.draw(arr[i].sprite);
}
