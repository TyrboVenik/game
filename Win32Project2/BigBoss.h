#pragma once
#include "Stones.h"
#include "Player.h"
#include "AllEnemyPatrons.h"
#include "AllEnemis.h"
#include "Sounds.h"

using namespace sf;

class BigBoss
{
public:
	Sounds sounds;
	Player player;
	AllPatrons A_P;
	
	AllEnemis A_E;
	AllEnemyPatrons A_E_P;
	
	Stones stones;

	BigBoss(Paints&paints):
		player(paints.tileShipR)
	{}
	void PauseMenu(RenderWindow&window,Sprite&tileFon, Text&text);

	void mayBePause(Text&text, RenderWindow&window,Sprite&tileFon);

	void waitPressR(RenderWindow&window, Text&text);
	
	void mayBeNew(Paints&paints, double time, double Score);


	void update(double time, Text&text);

	void Collision(Paints&paints);

	void draw(RenderWindow&window, Sprite&fon, Text&text);

	void timeScore(double time, Text& text, Player&player);
	
	void StartMenu(RenderWindow&window, Sprite&tileFon,Text&text);

	void menuDead(RenderWindow&window,Sprite&tileFon);
};

