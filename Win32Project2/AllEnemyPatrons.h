#pragma once
#include "EnemyPatron.h"
#include<vector>

using namespace std;

class AllEnemyPatrons
{
private:
	static const int MaxEnemyFireDeadTime = 500000;
public:
	vector<EnemyPatron> arr;

	void update(float time);

	void draw(RenderWindow&window);
};

