#include "Paints.h"



Paints::Paints()
{
	tileMetBang.loadFromFile("Bang.png");

	tileFireEnemyG.loadFromFile("EnemyFireG.png");
	tileFireEnemyV.loadFromFile("EnemyFireV.png");
	tileEnemyBang.loadFromFile("BigBang.png");

	tileEnemy.loadFromFile("Enemy.png");
	tileShipR.loadFromFile("ShipR.png");
	tileShipL.loadFromFile("ShipL.png");
	tileShipU.loadFromFile("ShipU.png");
	tileShipD.loadFromFile("ShipD.png");

	tileShot.loadFromFile("Shot.png");

	tileMet.loadFromFile("meteor.png");
	tileSetFon.loadFromFile("fon.png");

	font.loadFromFile("arial.ttf");
}

