#pragma once

#include <SFML/Graphics.hpp>
#include "Paints.h"
#include "AllEnemyPatrons.h"
#include <SFML/Audio.hpp>
#include "Sounds.h"
using namespace sf;

using namespace sf;

class Enemy
{
public:
	static const int MaxEnemyFireDeadTime = 500000;
	static const int MaxDeadTime = 1000000;
	static const int EnemyReload = 2000000;
	static const int MaxEnemyDeadTime = 1000000;
	
	float StarEnemyVel ;
	float EnemyVel;
	float maxEnemyVel;
	float Acceleration;
	

	FloatRect rect;
	Sprite sprite;
	float dx, dy;
	int hp;
	float DeadTime;
	float reloadTime;
	float MaxEnemyReload;
	int Ammunition;
	int Firing;
	float FiringTime;
	int dead;

	Enemy(Texture &image);

	void Shot(AllEnemyPatrons&A_P, Paints&paint, Sounds&sounds);

	void update(float time);

	void Collision();

	float getMaxDeadTime();
};


