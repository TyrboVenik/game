#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;



class Paints
{
public:
	Texture tileShipR;
	Texture tileShipL;
	Texture tileShipU;
	Texture tileShipD;
	Texture tileMet;
	Texture tileSetFon;
	Texture tileMetBang;
	Texture tileShot;
	Texture tileEnemy;
	Texture tileEnemyBang;
	Texture tileFireEnemyG;
	Texture tileFireEnemyV;
	Font font;


	Paints();
};

