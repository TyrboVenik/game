#pragma once
#include <vector>
#include "Enemy.h"
#include <SFML/Audio.hpp>

using namespace std;
using namespace sf;

class AllEnemis
{
private:
	static const int EnemyResp = 10;
public:
	vector<Enemy> arr;
	int k;
	

	AllEnemis() :k(0) {}

	void MayBeNew(Paints&paints, float Score);

	void update(float time);

	void draw(RenderWindow&window);

	void shot(AllEnemyPatrons& A_E_P, Paints&paints,Sounds&sounds);
};

