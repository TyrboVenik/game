#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;



class Patron
{
public:
	float ShotVel;
	FloatRect rect;
	Texture pnt;
	Sprite sprite;
	float dx, dy;
	int dead;
	float DeadTime;

	Patron(Texture &image, int duloX, int duloY);

	void update(float time);
};

