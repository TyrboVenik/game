#pragma once

#include "Meteor.h"
#include <vector>

using namespace std;

class Stones
{
private:
	static const int maxMetiors = 40;
	static const int  MaxDeadTime = 1000000;
	double MeteorResp;
	double Resp;
	int ready;
public:
	int kol;
	vector<Meteor> arr;

	Stones() :MeteorResp(400000),ready(1),kol(0),Resp(0) {}

	void NewMeteor(Texture &image,float Score);

	void MayBeNew(Texture &image,float Score);

	void update(float time,float Score);

	void drawAll(RenderWindow& window);
};

