#include "Enemy.h"


Enemy::Enemy(Texture &image)
{
	StarEnemyVel = 0.00007;
	EnemyVel = StarEnemyVel;
	maxEnemyVel = EnemyVel * 3;
	Acceleration = 0.1;
	Firing = 0;
	Ammunition = 0;
	reloadTime = 0;
	dead = 0;
	DeadTime = 0;
	MaxEnemyReload = EnemyReload;
	hp = 20;
	sprite.setTexture(image);

	int i = rand() % 4;

	if (i == 0)
	{
		dx = EnemyVel;
		dy = EnemyVel;
		rect.left = 0;
		rect.top = rand() % 650;
	}

	if (i == 1)
	{
		dx = EnemyVel;
		dy = EnemyVel;
		rect.left = rand() % 950;
		rect.top = 0;
	}

	if (i == 2)
	{
		dx = -EnemyVel;
		dy = EnemyVel;
		rect.left = 950;
		rect.top = rand() % 650;
	}

	if (i == 3)
	{
		dy = -EnemyVel;
		dx = EnemyVel;
		rect.left = rand() % 950;
		rect.top = 650;
	}

	rect.height = 80;
	rect.width = 80;
	//currentFrame = 0;
}

void Enemy::Shot(AllEnemyPatrons&A_P, Paints&paint, Sounds&sounds)
{
	if (!Ammunition)
		return;

	sounds.soundLazer.play();
	Ammunition = 0;
	reloadTime = 0;
	FiringTime = 0;
	Firing = 1;
	EnemyPatron PG(paint.tileFireEnemyG, 2100, 80);
	EnemyPatron PV(paint.tileFireEnemyV, 80, 2100);

	PG.sprite.setPosition(rect.left - 1015, rect.top);
	PV.sprite.setPosition(rect.left, rect.top - 1015);

	PG.rect.left = rect.left - 1015;
	PG.rect.top = rect.top + 40;
	PG.rect.height = 20;

	PV.rect.left = rect.left + 40;
	PV.rect.top = rect.top - 1015;
	PV.rect.width = 20;

	A_P.arr.push_back(PV);
	A_P.arr.push_back(PG);
}

void Enemy::update(float time)
{
	if (dead)
	{
		DeadTime += time;
		IntRect re(int(DeadTime * 12 / 1000000) * 200 + 50, 50, 200, 200);
		sprite.setTextureRect(re);
		return;
	}

	if (!Ammunition)
	{
		reloadTime += time;
		if (reloadTime > MaxEnemyReload)
		{
			if (MaxEnemyReload > 1000000)
			{
				MaxEnemyReload -= 100000;
			}
			if (abs(dx) < maxEnemyVel)
			{
				if (dx > 0)
					dx += StarEnemyVel* Acceleration;
				if (dx < 0)
					dx -= StarEnemyVel* Acceleration;
				if (dy > 0)
					dy += StarEnemyVel* Acceleration;
				if (dx < 0)
					dy -= StarEnemyVel* Acceleration;
			}
			Ammunition = 1;
		}
	}


	if (Firing)
	{
		FiringTime += time;
		if (FiringTime > MaxEnemyFireDeadTime)
			Firing = 0;
		else
			return;
	}

	if (rand() % 10000 == 0)
		dx = -EnemyVel;

	if (rand() % 10000 == 0)
		dy = -EnemyVel;


	Collision();

	rect.left += dx*time;

	rect.top += dy*time;

	sprite.setPosition(rect.left, rect.top);

}

void Enemy::Collision()
{
	if ((rect.left <= 0 && dx<0) || (rect.left >= 950 && dx>0))
		dx *= -1;

	if ((rect.top <= 0 && dy<0) || (rect.top >= 650 && dy>0))
		dy *= -1;
}

float Enemy::getMaxDeadTime()
{
	return MaxDeadTime;
}