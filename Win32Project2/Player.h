#pragma once
#include "Paints.h"
#include "AllPatrons.h"
#include <SFML/Graphics.hpp>
#include "Sounds.h"

using namespace sf;

class Player
	//:Military_Transport M_T;
{
private:
	float ShipVel;
	float MaxReloadTime;
public:
	float Score = 0;
	FloatRect rect;
	Sprite sprite;
	float dx, dy;
	int napr;
	int dead;
	int Ammunition;
	float reloadTime;
	int duloX;//1 ������ -1 �����
	int duloY;//1 ���� -1 �����
			  //float currentFrame;

	Player(Texture &image);

	void keys(Paints&paints, AllPatrons&A_P,Sounds&sounds);

	void Shot(Texture&image, AllPatrons&A_P, Sounds&sounds);

	void update(float time);

	void Collision();
};

