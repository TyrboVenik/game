#pragma once
#include "Patron.h"
#include <vector>

using namespace std;

class AllPatrons
{
public:
	vector<Patron> arr;

	void update(float time);
	void drawAll(RenderWindow& window);
};