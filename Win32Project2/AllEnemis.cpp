#include "AllEnemis.h"
#include "Sounds.h"

void AllEnemis::MayBeNew(Paints&paints, float Score)
{
	for (int i = 1; i<int(Score / EnemyResp) + 1; i++)
		if ((Score>i * EnemyResp) && (Score < (i * EnemyResp + 1)) && (k < i))
		{
			k++;
			Enemy newEnemy(paints.tileEnemy);
			arr.push_back(newEnemy);
			return;
		}
}

void AllEnemis::update(float time)
{
	for (int i = 0; i < arr.size(); ++i)
	{
		if (arr[i].dead)
			if (arr[i].DeadTime >= arr[i].getMaxDeadTime())
			{
				arr.erase(arr.begin() + i);
				--i;
				continue;
			}
		arr[i].update(time);
	}

}

void AllEnemis::draw(RenderWindow&window)
{
	for (int i = 0; i < arr.size(); ++i)
		window.draw(arr[i].sprite);
}

void AllEnemis::shot(AllEnemyPatrons& A_E_P, Paints&paints, Sounds&sounds)
{
	for (int i = 0; i < arr.size(); ++i)
		arr[i].Shot(A_E_P, paints, sounds);
}