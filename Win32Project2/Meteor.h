#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;



class Meteor
{
public:
	float StartMeteorVel;
	float Acceleration;
	float MeteorVel;
	FloatRect rect;
	Sprite sprite;
	float dx, dy;
	int dead;
	float DeadTime;
	float currentFrame;


	Meteor(Texture &image,float Score);

	void update(float time,float Score);

	void Collision();

};

