#include "AllPatrons.h"



void AllPatrons::update(float time)
{
	for (int i = 0; i < arr.size(); ++i)
	{
		if (arr[i].dead)
		{
			arr.erase(arr.begin() + i);
			continue;
		}
		arr[i].update(time);
	}
}

void AllPatrons::drawAll(RenderWindow& window)
{
	for (int i = 0; i < arr.size(); ++i)
		window.draw(arr[i].sprite);
}