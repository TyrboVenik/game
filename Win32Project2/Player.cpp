#include "Player.h"



Player::Player(Texture &image)
{
	ShipVel = 0.0004;
	MaxReloadTime = 80000;

	Score = 0;
	duloX = 1;
	duloY = 0;
	Ammunition = 1;
	reloadTime = 0;
	dead = 0;
	sprite.setTexture(image);
	rect = FloatRect(500, 300, 50, 50);
	napr = 1;
	dx = dy = ShipVel;
	//currentFrame = 0;
}

void Player::keys(Paints&paints, AllPatrons&A_P, Sounds&sounds)
{

	if (Keyboard::isKeyPressed(Keyboard::D))
	{
		duloX = 1;
		duloY = 0;
	}

	if (Keyboard::isKeyPressed(Keyboard::A))
	{
		duloX = -1;
		duloY = 0;
	}
	if (Keyboard::isKeyPressed(Keyboard::W))
	{
		duloX = 0;
		duloY = -1;
	}
	if (Keyboard::isKeyPressed(Keyboard::S))
	{
		duloX = 0;
		duloY = 1;
	}
	if ((Keyboard::isKeyPressed(Keyboard::S)) && (Keyboard::isKeyPressed(Keyboard::D)))
	{
		duloX = 1;
		duloY = 1;
	}
	if ((Keyboard::isKeyPressed(Keyboard::A)) && (Keyboard::isKeyPressed(Keyboard::W)))
	{
		duloX = -1;
		duloY = -1;
	}
	if ((Keyboard::isKeyPressed(Keyboard::D)) && (Keyboard::isKeyPressed(Keyboard::W)))
	{
		duloY = -1;
		duloX = 1;
	}
	if ((Keyboard::isKeyPressed(Keyboard::S)) && (Keyboard::isKeyPressed(Keyboard::A)))
	{
		duloY = 1;
		duloX = -1;
	}

	if (Keyboard::isKeyPressed(Keyboard::Left))
	{
		dx = -ShipVel;
		sprite.setTexture(paints.tileShipL);
	}
	if (Keyboard::isKeyPressed(Keyboard::Right))
	{
		dx = ShipVel;
		sprite.setTexture(paints.tileShipR);
	}
	if (Keyboard::isKeyPressed(Keyboard::Up))
	{
		dy = -ShipVel;
		sprite.setTexture(paints.tileShipU);
	}
	if (Keyboard::isKeyPressed(Keyboard::Down))
	{
		dy = ShipVel;
		sprite.setTexture(paints.tileShipD);
	}
	
	Shot(paints.tileShot, A_P,sounds);
	
}

void Player::Shot(Texture&image, AllPatrons&A_P,Sounds&sounds)
{
	if (!Ammunition)
		return;
	Ammunition = 0;
	sounds.soundShot.play();
	Patron P(image, duloX, duloY);

	P.rect.left = rect.left + duloX * 25;
	P.rect.top = rect.top + duloY * 25;

	A_P.arr.push_back(P);
}

void Player::update(float time)
{
	if (!Ammunition)
	{
		reloadTime += time;
		if (reloadTime > MaxReloadTime)
		{
			reloadTime = 0;
			Ammunition = 1;
		}
	}
	Collision();
	rect.left += dx*time;

	rect.top += dy*time;

	sprite.setPosition(rect.left, rect.top);
	dx = dy = 0;

}

void Player::Collision()
{
	if ((rect.left <= 0 && dx<0) || (rect.left >= 950 && dx>0))
		dx *= -1;

	if ((rect.top <= 0 && dy<0) || (rect.top >= 650 && dy>0))
		dy *= -1;
}