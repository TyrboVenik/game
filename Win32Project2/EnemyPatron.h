#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class EnemyPatron
{
public:
	FloatRect rect;
	Texture pnt;
	Sprite sprite;
	int dead;
	float DeadTime;

	EnemyPatron(Texture &image, const float& L, const float& R);

	void update(float time);
};