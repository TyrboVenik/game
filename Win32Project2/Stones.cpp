#include "Stones.h"
#include <iostream>

void Stones::NewMeteor(Texture &image,float Score)
{
	Meteor M(image,Score);
	arr.push_back(M);
}

void Stones::MayBeNew(Texture &image,float Score)
{
	if (kol>= maxMetiors)
		return;

	if (!ready)
		return;
	
	ready = 0;
	Resp = 0;
	kol++;
	NewMeteor(image,Score);
}

void Stones::update(float time,float Score)
{
	//cout << Resp << endl;
	if (!ready)
	{
		Resp += time;
		if (Resp > MeteorResp)
		{
			ready = 1;
			Resp = 0;
		}
	}
	for (int i = 0; i < arr.size(); ++i)
	{
		if (arr[i].dead)
			if (arr[i].DeadTime >= MaxDeadTime)
			{
				arr.erase(arr.begin() + i);
				kol--;
				continue;
			}
		arr[i].update(time,Score);
	}
}

void Stones::drawAll(RenderWindow& window)
{
	for (int i = 0; i < arr.size(); ++i)
		window.draw(arr[i].sprite);
}